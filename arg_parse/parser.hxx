#include <core/string.hxx>

struct Argument {
  String name;
  String description;

  void print() const { std::cout << "<" << name << ">"; }
  void describe() const { std::cout << name << ": " << description; };
};

struct Option {
  char short_option;
  String long_option;
  String description;

  Option(char so, String lo, String desc)
      : short_option(so), long_option(lo), description(desc) {}
  Option(String lo, String desc)
      : short_option(0), long_option(lo), description(desc) {}

  void print() const {
    std::cout << '-' << short_option << ", --" << long_option;
  }
};

struct OptionalArgument {
  char short_option;
  String long_option;
  String description;
  Argument arg;

  OptionalArgument(char so, String lo, String desc, Argument arg)
      : short_option(so), long_option(lo), description(desc), arg(arg) {}
  OptionalArgument(String lo, String desc, Argument arg)
      : short_option(0), long_option(lo), description(desc), arg(arg) {}

  void print() const {
    std::cout << '-' << short_option << ", --" << long_option << " ";
    arg.print();
  }
};

class Command;
class CommandBuilder;

class Command {
public:
  class Syntax {
  public:
    Syntax &with_option(char so, String lo, String desc) {
      m_options.push_back({so, lo, desc});
      return *this;
    }

    Syntax &with_optional_arg(char so, String lo, String desc, Argument arg) {
      m_optional_args.push_back({so, lo, desc, arg});
      return *this;
    }

    Syntax &with_arg(String name) {
      m_args.push_back({name});
      return *this;
    }

    void print() const {
      std::cout << m_name << " [sub-command] [options|optional-arguments] ";

      for (auto arg : m_args) {
        arg.print();
        std::cout << " ";
      }

      if (m_options.size()) {
        std::cout << "\noptions:";

        for (auto opt : m_options) {
          std::cout << "\n\t";
          opt.print();
        }
      }

      if (m_optional_args.size()) {
        std::cout << "\noptional-arguments:";
        for (auto opt_arg : m_optional_args) {
          std::cout << "\n\t";
          opt_arg.print();
        }
      }
    }

    explicit Syntax(String name) : m_name(name) {}

  private:
    std::vector<Option> m_options;
    std::vector<OptionalArgument> m_optional_args;
    std::vector<Argument> m_args;

    String m_name;

    friend class ArgParser;
  };

  using Value = String;
};

class ArgParser {
public:
  bool parse(int argc, char **argv) {

    if (m_sub_commands.size() > 0) {
      if (argc < 2) {
        printf("Expected sub-command!\n");
        return false;
      }

      String cmd_arg(argv[1]);
      for (auto cmd : m_sub_commands) {
        if (cmd.m_name.compare(cmd_arg) == 0) {
          std::cout << "Command: " << cmd.m_name << "\n";
        }
      }
    }
    return false;
  }

  void add_subcommand(Command::Syntax cmd_syntax) {
    m_sub_commands.push_back(cmd_syntax);
  };

  void print_help() { m_root_command.print(); }

  explicit ArgParser(String program_name)
      : m_root_command(program_name), m_sub_commands() {}

  Command::Syntax &edit_root_command() { return m_root_command; }

private:
  Command::Syntax m_root_command;
  std::vector<Command::Syntax> m_sub_commands;
};

int main(int argc, char **argv) {

  auto install_command =
      Command::Syntax("install")
          .with_option(
              'f', "force",
              "Ignores any warnings and forces the operation to complete.")
          .with_optional_arg('C', "compare-with",
                             "Compares the package supplied metadata.",
                             Argument("comparison"))
          .with_optional_arg('B', "backup-old",
                             "backs up all the old metadata.",
                             Argument("backup-store"))
          .with_arg("package");

  ArgParser arg_parser("test");

  arg_parser.edit_root_command()
      .with_option('v', "version", "Prints the version of the program")
      .with_option('h', "help", "Shows the help screen");

  arg_parser.add_subcommand(install_command);
  if (arg_parser.parse(argc, argv)) {

  } else {
    arg_parser.print_help();
  }

  return 0;
}

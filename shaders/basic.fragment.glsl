#version 330 core

smooth in vec3 frag_pos_out;


out vec4 frag_color;

void main() {
  frag_color = vec4(0.5, 0.0, 0.5, 1.0);
}

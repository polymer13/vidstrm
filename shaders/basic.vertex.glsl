#version 330 core

layout(location=0) in vec3 vert_pos;


smooth out vec3 frag_pos; 

void main() {
  frag_pos = vert_pos;
}

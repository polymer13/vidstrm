#pragma once

#include "core/error.hxx"
#include "core/file.hxx"
#include "core/maybe.hxx"
#include <GL/glew.h>
#include <optional>
#include <string>
#include <vector>

namespace shader {

enum struct ResourceType {
  SHADER,
  SHADER_PROGRAM,
};

struct ShaderIssue {
  enum struct SubType {
    SHADER_CREATE = 0,
    SHADER_PROGRAM_CREATE,
    COMPILE_ISSUE,
    LINK_ISSUE,
    FILE_NOT_ACCESSIBLE,
    COUNT
  } sub_type;

  ShaderIssue(SubType st, String&& msg): sub_type(st), message(msg) {}
  ShaderIssue(SubType st, const String& msg): sub_type(st), message(msg) {}
  ShaderIssue(const ShaderIssue& other): sub_type(other.sub_type), message(other.message) {}
  ~ShaderIssue() {};

  static const char* static_message(SubType type) {
    return error_static_messages(
      type,
      "Failed to create shader",
      "Failed to create shader program",
      "Failed to compile shader",
      "Failed to link shader program"
      "Failed to retrive and/or read shader source file"
      );
  }

  String to_string() {
    return ShaderIssue::static_message(sub_type);
  }

  String message;
};


enum struct ShaderType {
  VERTEX_SHADER = GL_VERTEX_SHADER,
  TESS_CONTROL_SHADER = GL_TESS_CONTROL_SHADER,
  TESS_EVALUATION_SHADER = GL_TESS_EVALUATION_SHADER,
  GEOMETRY_SHADER = GL_GEOMETRY_SHADER,
  FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
};

template <ShaderType ST> class Shader {
public:

  using ErrorValue = Error<ShaderIssue>;
  using ResultValue = Result<Shader<ST>, ShaderIssue>;

  ~Shader() { glDeleteShader(m_id); }

  static ResultValue create() {

    auto m_id = glCreateShader(static_cast<GLuint>(ST));
    if (m_id) {
      return Shader(m_id);
    } else {
      return ErrorValue(
        ShaderIssue::SubType::SHADER_CREATE,
        "Shader cannot be create, check OpenGL context!"
        );
    }
  }

  auto get_id() const -> GLuint { return m_id; }

  // FIXME FIXME FIXME FIXME
  static ResultValue load_from_file(const String& path)  {
    auto src =read_file_to_string(path);
    if (src) {
      return load_from_string(*src);
    } else {
      return Error(ShaderIssue::SubType::FILE_NOT_ACCESSIBLE, src.error_string());   
    }
  }

  // In the future we might do some preprocessing on shaders.
  static ResultValue load_from_string(const String &buf) { return compile(buf); }

  static ResultValue compile(const String &source) {

    auto shader = Shader::create();

    auto compile_error = [](auto msg) {return Error<ShaderIssue>(ShaderIssue::SubType::COMPILE_ISSUE, msg);};

    if (source.empty()) {
      return compile_error("Provided shader source is empty!");
    }

    const char *source_cstr = static_cast<const char*>(source.get_buffer().get());
    const GLint source_len = source.size();

    glShaderSource(shader.get_id(), 1, &source_cstr, &source_len);

    glCompileShader(shader.get_id());

    GLint compile_status = 0;

    glGetShaderiv(shader.get_id(), GL_COMPILE_STATUS, &compile_status);

    if (compile_status == GL_FALSE) {

      GLint log_size = 0;
      glGetShaderiv(shader.get_id(), GL_INFO_LOG_LENGTH, &log_size);

      String message;
      message.expand(log_size);

      glGetShaderInfoLog(shader.get_id(), log_size, &log_size, message.get_mutable_buffer().get());

      return compile_error(message);
    }
  }

private:
  explicit Shader(GLuint id) : m_id(id){};
  GLuint m_id;
};

using VertexShader = Shader<ShaderType::VERTEX_SHADER>;
using VertexShader = Shader<ShaderType::VERTEX_SHADER>;
using TessControlShader = Shader<ShaderType::TESS_CONTROL_SHADER>;
using TessEvaluationShader = Shader<ShaderType::TESS_EVALUATION_SHADER>;
using GeometryShader = Shader<ShaderType::GEOMETRY_SHADER>;
using FragmentShader = Shader<ShaderType::FRAGMENT_SHADER>;

class ShaderProgram {
public:

  static Result<ShaderProgram, ShaderIssue> create();
  static Result<ShaderProgram, ShaderIssue> link(VertexShader, FragmentShader);
  ~ShaderProgram();

  ShaderProgram(const ShaderProgram& other): m_id(other.m_id) {}

  GLuint get_id() const { return m_id; }
private:
  explicit ShaderProgram(GLuint id);
  GLuint m_id;
};

} // namespace shader

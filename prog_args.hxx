#pragma once

#include "core/error.hxx"
#include "core/string.hxx"
#include "core/result.hxx"
#include <boost/format.hpp>
#include <vector>
#include <cstring>
#include <iostream>

struct  ProgramArgsIssue {

  enum struct SubType {
    MISSING_ARGUMENT_FOR_OPTION,
    INVALID_ARGUMENT,
    UNKNOWN_SHORT_OPTION,
    COUNT,
  } sub_type;

  union {
    char unknown_short_option;
    String unknown_long_option;
    String invalid_argument;
  }

  ProgramArgsIssue(SubType st): sub_type(st) {}
  
  static const char* static_message(SubType st) {
    return error_static_messages(
      st,
      "Argument missing for option",
      "Argument is invalid",
      "Unknown specified option",
      );
  }

  String to_string() {
    return static_message(sub_type);
  }

};

struct ProgramArgs {
  std::string program_name;

  ProgramArgs() : program_name(CONFIG_APP_NAME), args() {}

  enum struct Option {
    BASE_DIRECTORY = 'd',
    VERBOSE_OUTPUT= 'v',
  };

  enum struct ArgumentType {
    PATH,
    NAME, 
  };

  static constexpr const char* valid_short_options = "dv";

  struct LongOption {
    Option option;
    const char* name;
  };

  static constexpr LongOption long_options[] = {
    { Option::BASE_DIRECTORY, "base-dir" },
    { Option::VERBOSE_OUTPUT, "verbose" },
  };

  Maybe<ProgramArgsIssue> parse_short_options(
    const char* str,
    std::vector<Option> &opts,
    std::vector<ArgumentType> &args
    ) {


    while(str++) {
      if(std::strchr(valid_short_options, *arg)) {
        opts.push_back(static_cast<Option>(*arg));
      }
      else {
        return Error<ProgramArgsIssue>(ProgramArgsIssue::SubType::UNKNOWN_OPTION);
      }
    }

  }

  Maybe<ProgramArgsIssue> parser_long_options(
    const char* str,
    Option &opts,
    Maybe<ArgumentType> &expect
    ) {

    auto look_up = [&](const char* s) -> Option {
      for (int i = 0; i < sizeof(long_options)/sizeof(LongOption); i++) {
        if(strncmp(s, long_options[i].name, strlen(long_options[i].name))) {
          return long_options[i].option;
        }
      }
    };

    look_up(str);
  }

  enum struct ParseState {
    EXPECT_OPTION_OR_ARGUMENT,
    EXPECT_SHORT_OPTION,
    EXPECT_LONG_OPTION,
    EXPECT_ARGUMENT,
    EXPECT_OPTION_ARGUMENT
  };

  Error<ProgramArgsIssue> parse (int argc, char** argv) {

    std::vector<Options> options;
    std::vector<ArgumentType> expected;
    ParseState state = ParseState::EXPECT_OPTION_OR_ARGUMENT;
    const char* arg = argv[0];

    using Issue = ParseArgsIssue::SubState;

    auto make_error = [](Issue st) {
      return Error<ParseArgsIssue>(st);
    };

    for(int i = 0; i < argc;) {
      if(*arg == '\0') {
        i++;

        // Premature ending
        switch(state) {
        case ParseState::EXPECT_LONG_OPTION: 
          return make_error(Issue::EXPECTED_OPTION);
          return 
          break;
        case ParseState::EXPECT_SHORT_OPTION: 
          return make_error(Issue::EXPECTED_OPTION);
          break;
        }
        continue;
      }

      switch(state) {
      case ParseState::EXPECT_OPTION_OR_ARGUMENT: 
        if(strncmp("--", argv[i], 2) == 0) {
          argv += 2;
          state = ParseState::EXPECT_LONG_OPTION;
          break;
        }
        else if (strncmp("-", argv[i], 1) == 0) {
          argv += 1;
          state = ParseState::EXPECT_SHORT_OPTION;
          break;
        }
        break;
      case ParseState::EXPECT_SHORT_OPTION:
        if(auto err = parse_short_options(arg, options, expected)) {
          // Failed parsing
          return *err;
        }
        if(expected.size() > 0) {
          state = ParseState::EXPECTED_OPTION_ARGUMENT;
          break;
        }
        break;
      case ParseState::EXPECT_LONG_OPTION:
        // Parse long argument
        break;
      case ParseState::EXPECTED_OPTION_ARGUMENT:
        // Parse arguments supplied to options in the options were specified
        break;
      }
    }
  }
};

#pragma once

#include <string>

namespace resource {

enum struct ResourceType {

  SHADER_SOURCE_FILE = 0,
  IMAGE_FILE,

};

auto resource_code_string(ResourceType rt) -> const char *;

} // namespace resource

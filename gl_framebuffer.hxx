#ifndef __GL_FRAMEBUFFER_H_
#define __GL_FRAMEBUFFER_H_
#include "framebuffer.hxx"

class GL_Framebuffer : public Framebuffer {
public:
  GL_Framebuffer();
  virtual ~GL_Framebuffer() override;

  virtual void init() override;
  virtual void reinit() override;
  virtual void close() override;
  virtual void copy(Pixmap const &pixels) override;
  virtual void resize_notify() override;
};

#endif // __GL_FRAMEBUFFER_H_

#ifndef __FRAMEBUFFER_H_
#define __FRAMEBUFFER_H_
#include "rect.hxx"

struct Pixmap;

struct Framebuffer {
  Rect region;

  virtual ~Framebuffer();

  virtual void init() = 0;
  virtual void reinit() = 0;
  virtual void close() = 0;
  virtual void copy(Pixmap const &pixels) = 0;
  virtual void resize_notify() = 0;

  void operator=(Rect const &rect) {
    region = rect;
    resize_notify();
  }
};

#endif // __FRAMEBUFFER_H_

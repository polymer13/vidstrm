#include "rect.hxx"

Rect::Rect() : width(0), height(0), x(0), y(0) {}

Rect::Rect(uint32_t w, uint32_t h, int32_t x, int32_t y)
    : width(w), height(h), x(x), y(y) {}

void Rect::set_top(int32_t ny) {
  y = ny;
}
void Rect::set_bottom(int32_t ny) {
  height = ny - y;
}
void Rect::set_left(int32_t nx) {
  x = nx;
}
void Rect::set_right(int32_t nx) {
  width = nx - x;
}

void Rect::resize(uint32_t w, uint32_t h) {
  width = w;
  height = h;
}

void Rect::place(int32_t nx, int32_t ny) {
  x = nx;
  y = ny;
}

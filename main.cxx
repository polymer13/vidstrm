#include "prog_args.hxx"
#include "shader.hxx"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstring>
#include <iostream>
#include <map>

int main(int argc, char **argv) {
  ProgramArgs pargs;

  try {
    pargs.parse(argc, argv);
  } catch (GeneralError &er) {
    std::cout << er.message << '\n';
    pargs.print_usage();
    return -1;
  }

  GLFWwindow *window;

  if (!glfwInit())
    return -1;

  window = glfwCreateWindow(640, 480, CONFIG_APP_NAME, NULL, NULL);
  if (!window) {
    glfwTerminate();
    exit(1);
  }

  glfwMakeContextCurrent(window);

  if (glewInit() != GLEW_OK) {
    std::cout << "Failed to load OpenGL extensions using GLEW!\n";
    glfwTerminate();
    exit(1);
  }

  glClearColor(0.5f, 0.0f, 0.5f, 1.0f);

  auto vs = shader::VertexShader::create();
  assert(vs);
  auto fs = shader::FragmentShader::create();
  assert(fs);

  PathBuf path(pargs.args["base-dir"]);

  path.append("shaders/basic.vertex.glsl");

  std::cout << "VERTEX_SHADER_PATH: " << path << std::endl;

  vs->load_from_file(path);

  path.replace_filename("shaders/basic.fragment.glsl");

  std::cout << "FRAGMENT_SHADER_PATH: " << path << std::endl;

  fs->load_from_file(path);

  shader::ShaderProgram shader_prog(*vs, *fs);

  while (!glfwWindowShouldClose(window)) {
    glClear(GL_COLOR_BUFFER_BIT);

    glfwSwapBuffers(window);

    glfwPollEvents();
  }

  glfwTerminate();
  return 0;
}

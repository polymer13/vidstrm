#include "resource.hxx"

auto resource::resource_code_string(ResourceType rt) -> const char * {
  using RT = resource::ResourceType;

  switch (rt) {
  case RT::SHADER_SOURCE_FILE:
    return "SHADER_SOURCE_FILE";
  default:
    return "Unregistered Resource Type (BUG)";
  }
}

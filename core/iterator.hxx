#pragma once
#include <core/maybe.hxx>
#include <core/pair.hxx>

template <class T> concept Iterator = requires(T iter) {
  { iter.next() }
  ->std::same_as<Maybe<typename T::Element>>;
  { iter++ }
  ->std::same_as<&T>;
};

template <class T> concept Iterable = requires(T iterable) {
  { iterable.begin() }
  ->Iterator;
  { iterable.end() }
  ->Iterator;
};

template <Iterator FirstIter, Iterator SecondIter> struct ZippedIterator {

  using Self = ZippedIterator<FirstIter, SecondIter>;
  using iterator_category = std::forward_iterator_tag;
  using Element =
      Pair<typename FirstIter::Element, typename SecondIter::Element>;
  using Pointer = Element *;
  using Reference = Element &;

  explicit ZippedIterator(FirstIter i1, SecondIter i2)
      : first_iter(i1), second_iter(i2), item(*first_iter, *second_iter) {}

  Self &operator++() {
    first_iter++;
    second_iter++;
    item.first = *first_iter;
    item.second = *second_iter;
    return *this;
  }

  Reference operator*() { return item; }
  Pointer operator->() { return &item; }

  friend bool operator==(const Self &lhs, const Self &rhs) {
    return lhs.first_iter == rhs.first_iter &&
           lhs.second_iter == rhs.second_iter;
  }

  friend bool operator!=(const Self &lhs, const Self &rhs) {
    return lhs.first_iter != rhs.first_iter ||
           lhs.second_iter != rhs.second_iter;
  }

private:
  FirstIter first_iter;
  SecondIter second_iter;
  Element item;
};

template <Iterable First, Iterable Second> struct Zip {

  Zip(First &fi, Second &si) : first_iterable(fi), second_iterable(si) {}

  auto begin() {
    return ZippedIterator<decltype(first_iterable.begin()),
                          decltype(second_iterable.begin())>(
        first_iterable.begin(), second_iterable.begin());
  }

  auto end() {
    return ZippedIterator<decltype(first_iterable.end()),
                          decltype(second_iterable.end())>(
        first_iterable.end(), second_iterable.end());
  }

private:
  First &first_iterable;
  Second &second_iterable;
}

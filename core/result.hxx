#pragma once

#include <cassert>
#include <functional>
#include <utility>
#include "maybe.hxx"
#include "string.hxx"
#include "error.hxx"
#include <concepts>

template<typename T>
concept ResultValue = std::copy_constructible<T>;

template <ResultValue Value, IssueType Issue> class Result {

  union {
    Value _value;
    Error<Issue> _err;
  };

public:
  Result(Value &&value) : _value(std::move(value)), m_is_value(true) {}
  Result(Issue &&err) : _err(Error<Issue>(std::move(err))), m_is_value(false) {}
  Result(Error<Issue> &&err) : _err(std::move(err)), m_is_value(false) {}

  Result(const Result& rhs): m_is_value(rhs.m_is_value) {
    if(rhs.m_is_value) {
      _value = rhs._value;
    }
    else {
      _err = rhs._err;
    }
  }

  ~Result() {
    if(m_is_value) {
      (&_value)->~Value();
    }
    else {
      (&_err)->~Error<Issue>();
    }
  };

  Value *operator->() {
    assert(m_is_value);
    return (&_value);
  }

  Value *operator*() {
    assert(m_is_value);
    return &_value;
  }

  bool is_value() { return m_is_value; }

  bool operator==(bool rhs) { return rhs == m_is_value; }
  operator bool() { return m_is_value; }

  String error_string() {
    if(!m_is_value) {
      return _err.to_string();
    }
    else {
      return "No errors produces by result";
    }
  }

private:
  bool m_is_value;
};

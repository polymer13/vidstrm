#include "file.hxx"
#include <fstream>
#include <iostream>
#include <filesystem>

namespace fs = std::filesystem;

using _Issue = FileSystemIssue::SubType;
using FsError = Error<FileSystemIssue>;


// FIXME: Read the characters with better stream, possibly implement one.
Result<String, Error<FileSystemIssue>> read_file_to_string(const String& path)  {
  String str;
  const char* path_str = static_cast<const char*>(path.get_buffer().get());
  auto f = std::ifstream(path_str, std::ios::in | std::ios::binary);
  if (!f.good()) {
    return FsError(_Issue::INACESSIBLE_FILE, path);
  }

  const auto fsize = static_cast<std::streamsize>(fs::file_size(path_str));

  str.expand(fsize);
  char* buffer_ptr = str.get_mutable_buffer().get();
  //FIXME: This looks very ugly, please do something else.
  f.read(buffer_ptr, fsize);

  if (f.gcount() != fsize) {
    std::cout << "Partially read file " << path_str << std::endl;
    return FsError(_Issue::INCOMPLETE_READ, path);
  }
  return std::move(str);
}

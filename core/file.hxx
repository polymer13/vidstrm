#pragma once

#include "result.hxx"
#include "string.hxx"
#include "error.hxx"

class FileSystemIssue {
public: 
  enum struct SubType {
    INACESSIBLE_FILE,
    ACCESS_DENIED,
    PERMISSION_CONFLICT,
    READ_FAILED,
    INCOMPLETE_READ,
    WRITE_FAILED,
    INCOMPLETE_WRITE,
    COUNT,
  } sub_type;

  FileSystemIssue(String&& pb): path_buf(pb) {}
  FileSystemIssue(const String& pb): path_buf(pb) {}

  SubType get_sub_type() { return sub_type; };

  static const char* static_message(SubType type) {
    return error_static_messages(
      type,
      "File not accessible",
      "Access Denied",
      "Permissions not matching access rights"
      );
  }
 
  // FIXME: Convert the path to string more efficiently and potentially write custom path class.
  String to_string() {
    // return (boost::format("%1%: %2%") % path_buf.get_buffer().get() % static_messages).str();
    return FileSystemIssue::static_message(sub_type);
  }

private:
  String path_buf;
};

Result<String, FileSystemIssue> read_file_to_string(const String& path);

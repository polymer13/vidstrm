#pragma once

#define ASSERT(x)                                                              \
  do {                                                                         \
    if (!(x)) {                                                                \
      std::cerr << __FILE__ << ":" << __LINE__ << ":\nASSERTION FAILED: "      \
                << "\"" << (#x) << "\""                                        \
                << "\nPROCESS ABORTED!" << std::endl;                          \
      abort();                                                                 \
    }                                                                          \
  } while (0);

#define ASSERT_EQ(x, eq)                                                       \
  do {                                                                         \
    auto res = (x);                                                            \
    if (res != (eq)) {                                                         \
      std::cerr << __FILE__ << ":" << __LINE__ << ":\nASSERTION FAILED: "      \
                << "\"" << (#x) << "\"\nEXPECTED: " << (#eq)                   \
                << "\nGOT: " << res << "\nPROCESS ABORTED!" << std::endl;      \
      abort();                                                                 \
    }                                                                          \
  } while (0);

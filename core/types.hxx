#pragma once

#include <cinttypes>

#if _WIN64 || _WIN32
#if _WIN64
#define PLATFORM_ARCH_64BIT
#else 
#define PLATFORM_ARCH_32BIT
#endif
#endif


#if __GNUC__
#if __x86_64__ || __ppc64__
#define PLATFORM_ARCH_32BIT
#else
#define PLATFORM_ARCH_32BIT
#endif
#endif


#ifdef PLATFORM_ARCH_32BIT
using isize = int32_t;
using usize = uint32_t;
#elif PLAFORM_ARCH_64BIT
using isize = int64_t;
using usize = uint64_t;
#endif

#pragma once

#include "string.hxx"
#include <concepts>

template <typename T> concept IssueType = requires(T a) {
  { T::SubType::COUNT }
  ->std::same_as<typename T::SubType>;
  { T::static_message(a.sub_type) }
  ->std::same_as<const char *>;
};

template <IssueType Issue> struct Error {
public:
  using SubType = typename Issue::SubType;

  template <typename... _Args>
  Error(SubType st, _Args... args) : issue(st, args...) {}

  const char *static_message() {
    return Issue::static_message(issue.get_sub_type());
  }

  String to_string() { return issue.to_string(); }

private:
  Issue issue;
};

template <class Type, typename... _Args>
const char *error_static_messages(Type val, _Args... args) {
  static const char *MSG[] = {args...};

  int i = static_cast<int>(val);
  if (i >= static_cast<int>(Type::COUNT) || i < 0) {
    return "UNKNOW ERROR (BUG!)";
  }

  return MSG[i];
}

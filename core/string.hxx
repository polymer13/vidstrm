#pragma once

#include "maybe.hxx"
#include "types.hxx"
#include <memory>
#include <ostream>
#include <string>

class String {
public:
  using Char = char;
  using CharFull = uint32_t;
  using BufferPtr = std::shared_ptr<Char>;
  using BufferConstPtr = std::shared_ptr<const Char>;

  class CharIter {
  public:
    using iterator_category = std::forward_iterator_tag;
    using Element = String::CharFull;
    using Pointer = const Element *;
    using Reference = const Element &;

    CharIter()
        : m_base_ptr(nullptr), m_current_char(Nothing), m_offset(0), m_dest(0),
          m_index(0) {}

    CharIter(BufferConstPtr ptr, isize offset, isize dest)
        : m_base_ptr(ptr), m_current_char('\0'), m_offset(offset), m_dest(dest),
          m_index(0) {
      m_current_char = next();
    }

    Reference operator*();
    Pointer operator->();

    CharIter &operator++();

    friend bool operator==(const CharIter &, const CharIter &);
    friend bool operator!=(const CharIter &, const CharIter &);

    Maybe<String::CharFull> next();

  private:
    BufferConstPtr m_base_ptr;
    Maybe<CharFull> m_current_char;
    isize m_offset;
    isize m_dest;
    isize m_index;
  };

  String() : m_buffer(), m_size(0), m_capacity(0) {}
  String(const char *s);
  String(const std::string &s);
  String(String &&s);
  String(const String &s);
  ~String(){};

  friend void swap(String &lhs, String &rhs) {
    using std::swap;

    swap(lhs.m_buffer, rhs.m_buffer);
    swap(lhs.m_size, rhs.m_size);
    swap(lhs.m_capacity, rhs.m_capacity);
  }

  Maybe<CharIter> sub_string(isize start, isize end) const;
  CharIter iter() const;

  bool empty() const { return m_size == 0; }

  void expand(isize c);

  BufferConstPtr get_buffer() const;
  BufferPtr get_mutable_buffer();

  isize index_to_offset(isize idx) const;
  isize offset_to_index(isize idx) const;

  bool is_empty() const { return m_size == 0; }
  isize size() const { return m_size; }
  usize unsigned_size() const { return static_cast<::usize>(m_size); }
  bool starts_with(const char *str) const;
  bool starts_with(const String &str) const;

  CharFull operator[](isize idx);
  String &operator=(String rhs) {
    swap(*this, rhs);
    return *this;
  }

  isize compare(const String &rhs) const;

  CharIter begin() const { return iter(); }
  CharIter end() const { return CharIter(); }

private:
  void expand_no_copy(isize size);
  static BufferPtr alloc(isize size);

  BufferPtr m_buffer;
  isize m_size;
  isize m_capacity;
};

std::ostream &operator<<(std::ostream &strm, const String &str);

template <typename T> concept ToString = requires(T a) {
  { a.to_string() }
  ->std::same_as<String>;
};

#pragma once

template <typename FirstItem, typename SecondItem> struct Pair {
  FirstItem first;
  SecondItem second;

  Pair(const FirstItem &a, const SecondItem b) : first(a), second(b) {}

  Pair<SecondItem, FirstItem> swapped() { return Pair(second, first); }
};

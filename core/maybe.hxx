#pragma once

#include "assert.hxx"
#include <functional>
#include <iostream>
#include <utility>

struct NothingType {
  enum class Construct { Token };

  explicit constexpr NothingType(Construct) {}
};

inline constexpr NothingType Nothing{NothingType::Construct::Token};

template <class T> class Maybe {
  bool _is_ok;
  union {
    T _data;
  };

public:
  Maybe(T &&_data) : _is_ok(true), _data(std::move(_data)) {}
  Maybe() : _is_ok(false) {}
  Maybe(NothingType &&) : _is_ok(false) {}
  Maybe(NothingType) : _is_ok(false) {}

  T &operator*() {
    ASSERT(_is_ok);
    return _data;
  }

  T *operator->() {
    ASSERT(_is_ok);
    return &_data;
  }

  T &&take() {
    ASSERT(_is_ok);
    return std::move(_data);
  }

  T take_or(std::function<T && ()> cb) {
    if (_is_ok) {
      return std::move(_data);
    } else {
      return std::move(cb());
    }
  }

  T take_or_default(T &&d) {
    if (_is_ok) {
      return std::move(_data);
    } else {
      return std::move(d);
    }
  }

  ~Maybe() {}

  bool is_ok() const { return _is_ok; }
  operator bool() const { return _is_ok; }
};

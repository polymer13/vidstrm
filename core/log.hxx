#pragma once

#include "file.hxx"

namespace msg {

struct Message {
  char data[256];

  Message() : data{0} {}
  Message(const char *msg);
};

class Logger {

  Message message_buf[32];

  enum struct MessageCategory { WARNING, NONE, ERROR } category;

  enum struct MessageType {
    FILE_ERROR,
    ALLOC_ERROR,
    RESOURCE_ERROR,
  } type;

  explicit Logger(PathRef dump_file, bool enable_stdout);
  ~Logger();
};

} // namespace msg

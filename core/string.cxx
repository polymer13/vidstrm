#include "assert.hxx"
#include "string.hxx"
#include <cstring>
#include <iostream>
#include <string>

String::String(String &&s)
    : m_buffer(std ::move(s.m_buffer)), m_size(s.m_size),
      m_capacity(s.m_capacity) {}

String::String(const String &s)
    : m_buffer(alloc(s.m_size)), m_size(s.m_size), m_capacity(s.m_size) {
  std::copy(s.m_buffer.get(), s.m_buffer.get() + m_size, m_buffer.get());
}

String::String(const char *s) : m_buffer(), m_size(0), m_capacity(0) {
  std::size_t len = strlen(s);
  expand_no_copy(len);

  std::copy(s, s + len, m_buffer.get());
  m_size = len;
}

String::String(const std::string &s) : m_buffer(), m_size(0), m_capacity(0) {
  expand_no_copy(s.size());

  const char *ptr = s.c_str();
  std::copy(ptr, ptr + s.size(), m_buffer.get());
  m_size = s.size();
}

void String::expand_no_copy(isize nc) {
  ASSERT(nc > 0);
  if (m_capacity < nc) {
    m_capacity = nc;
    auto ns = alloc(nc);
    m_buffer = std::move(ns);
  }
}

void String::expand(isize nc) {
  ASSERT(nc > 0);
  if (m_capacity < nc) {
    m_capacity = nc;
    auto ns = alloc(nc);
    auto ptr = m_buffer.get();
    std::copy(ptr, ptr + nc, ns.get());
    m_buffer = std::move(ns);
  }
}

String::BufferPtr String::alloc(isize size) {
  ASSERT(size > 0);
  return std::shared_ptr<String::Char>(new String::Char[size + 4],
                                       std::default_delete<String::Char[]>());
}

String::BufferConstPtr String::get_buffer() const {
  return static_cast<BufferConstPtr>(m_buffer);
}

String::BufferPtr String::get_mutable_buffer() { return m_buffer; }

std::ostream &operator<<(std::ostream &strm, const String &str) {
  if (str.size() <= 0)
    return strm;
  strm.write(static_cast<const char *>(str.get_buffer().get()), str.size());
  return strm;
}

bool String::starts_with(const char *str) const {
  auto len = static_cast<isize>(strlen(str));
  if (len > m_size) {
    return false;
  } else {
    if (std::memcmp(str, m_buffer.get(), len) == 0) {
      return true;
    } else {
      return false;
    }
  }
}

bool String::starts_with(const String &str) const {
  if (str.is_empty() && this->is_empty()) {
    return true;
  }

  if (str.is_empty() || this->is_empty()) {
    return false;
  }

  if (str.size() <= m_size) {
    auto c = str.compare(str);
    return c == str.size();
  } else {
    return false;
  }
}

// FIXME: Implement custom memcmp that accounts for multibyte characters to
// eliminate the use of offset_to_index
isize String::compare(const String &rhs) const {

  auto this_iter = this->iter();
  auto rhs_iter = rhs.iter();

  // while (auto this_char = this_iter->next() &&auto rhs_char =
  //            rhs_iter->next()) {
  // }
  abort();
}

Maybe<String::CharIter> String::sub_string(isize p1, isize p2) const {
  ASSERT(p1 >= 0 || p2 >= 0);

  p1 = std::min(p1, m_size);
  p2 = std::min(p2, m_size);

  auto start = std::min(p1, p2);
  auto end = std::max(p1, p2);

  start = index_to_offset(start);
  end = index_to_offset(end);

  return CharIter(m_buffer, start, end);
}

String::CharIter String::iter() const { return CharIter(m_buffer, 0, m_size); }

String::CharFull char_extract_parts(const char *c, isize parts) {
  ASSERT(parts < 5);
  String::CharFull val = 0;

  auto char_part = [&](auto off) {
    return (static_cast<String::CharFull>(*(c + off)) << off);
  };

  switch (parts) {
  case 1:
    val = char_part(0);
    break;
  case 2:
    val = char_part(0) | char_part(1);
    break;
  case 3:
    val = char_part(0) | char_part(1) | char_part(2);
    break;
  case 4:
    val = char_part(0) | char_part(1) | char_part(2) | char_part(3);
    break;
  default:
    return '\0';
  }
  return val;
}

isize char_size(char c) {
  auto sz = ((c | 0xF0) >> 4);

  switch (sz) {
  case 0b0000:
    sz = 1;
  // 0b1000 reserved for continuation of characters
  case 0b1100:
    sz = 2;
  case 0b1110:
    sz = 3;
  case 0b1111:
    sz = 4;
  default:
    sz = 1;
  }
  return sz;
}

isize String::offset_to_index(isize off) const {
  ASSERT(off >= 0);
  ASSERT(off < m_size);

  const Char *buf = m_buffer.get();

  isize idx = 0;

  while (off > 0) {
    idx += 1;
    off -= char_size(buf[idx]);
  }

  return idx;
}

isize String::index_to_offset(isize idx) const {
  ASSERT(idx >= 0);
  ASSERT(idx < m_size);

  if (idx >= m_size) {
    return '\0';
  }

  auto ptr = m_buffer.get();
  auto offset = 0;

  while (--idx >= 0) {
    offset += char_size(ptr[offset]);
  }

  return offset;
}

String::CharFull String::operator[](isize idx) {
  auto offset = index_to_offset(idx);
  auto ptr = m_buffer.get();
  auto c_ptr = ptr + offset;
  return char_extract_parts(c_ptr, char_size(*c_ptr));
}

String::CharIter::Reference String::CharIter::operator*() {
  return *m_current_char;
}

String::CharIter::Pointer String::CharIter::operator->() {
  return &*m_current_char;
}

bool operator==(const String::CharIter &lhs, const String::CharIter &rhs) {
  // if (!lhs.m_current_char && !rhs.m_current_char) {
  //   std::cout << "Both iterators have no characters\n";
  //   return true; // Not characters;
  // } else {
  //   return (lhs.m_base_ptr == rhs.m_base_ptr && lhs.m_index == rhs.m_index);
  // }

  return (lhs.m_current_char.is_ok() == rhs.m_current_char.is_ok());
}

bool operator!=(const String::CharIter &lhs, const String::CharIter &rhs) {
  // if (!lhs.m_current_char && !rhs.m_current_char) {
  //   std::cout << "Both iterators have no characters\n";
  //   return false;
  // } else {
  //   return (lhs.m_base_ptr != rhs.m_base_ptr && lhs.m_index != rhs.m_index);
  // }
  return (lhs.m_current_char.is_ok() != rhs.m_current_char.is_ok());
}

String::CharIter &String::CharIter::operator++() {
  m_current_char = next();
  return *this;
}

Maybe<String::CharFull> String::CharIter::next() {
  if (m_index >= m_dest) {
    return Nothing;
  }

  auto ptr = m_base_ptr.get();
  auto c_ptr = ptr + m_offset;
  auto s = char_size(*c_ptr);
  m_index++;
  m_offset += s;
  return char_extract_parts(c_ptr, s);
}

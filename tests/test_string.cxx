#include <cassert>
#include <core/assert.hxx>
#include <core/string.hxx>
#include <iostream>
#include <string>

struct Fruit {

  Fruit(std::string n) : name(n) {}

  String to_string() const { return String(name); }

private:
  std::string name;
};

String convert_to_string(const ToString auto &a) { return a.to_string(); };

int main(int argc, char **argv) {

  String str("Hello there mate!");
  String str2 = "Sauce";
  String str3(std::string("Wow man, pretty cool!"));

  String str4 = str2 = str = "Well this worked!";

  str3 = "Goodness";
  str = "Niceness";

  auto c = str3[3];
  std::wcout << "str3[4] = " << (wchar_t)c << std::endl;

  auto i = str3.sub_string(0, 2);
  while (auto c = i->next()) {
    std::wcout << (wchar_t) * *c << "," << std::endl;
  }

  auto i2 = str4.iter();
  printf("Fuck you man !");

  while (auto c = i2->next()) {
    std::wcout << (wchar_t) * *c << ".";
  }

  std::cout << "\n";
  std::cout << str << std::endl;

  Fruit apple("Apple");
  String apple_string = convert_to_string(apple);
  String some_text = "Once upon a time there was an evil voivode who liked to "
                     "impail his enemies!";

  std::cout << "Compare apples to apps: "
            << apple_string.compare(String("Application")) << std::endl;

  ASSERT_EQ(apple_string.compare(String("Apple")), 0);
  ASSERT_EQ(apple_string.compare(String("apple")), 1);
  ASSERT_EQ(apple_string.compare(String("Application")), -5);
  ASSERT_EQ(apple_string.compare(String("Appl")), 4);

  ASSERT_EQ(some_text.starts_with("Once upon a time"), true);
  ASSERT_EQ(some_text.starts_with(String("Once upon a time")), true);

  return 0;
}

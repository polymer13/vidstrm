#include <core/iterator.hxx>
#include <core/string.hxx>
int main(int argc, char **argv) {

  String letters1("Hlowrd"), letters2("el ol!");

  // auto iter = ZippedIterator(*letters1.iter(), *letters2.iter());

  // while (auto c = iter
  //   std::wcout << static_cast<wchar_t>(*c->first)
  //              << static_cast<wchar_t>(*c->second);
  // }

  for (auto c : letters1) {
    std::wcout << static_cast<wchar_t>(c);
  }

  for (auto c : Zip(letters1, letters2)) {
    std::wcout << static_cast<wchar_t>(c.first);
    std::wcout << static_cast<wchar_t>(c.second);
  }

  std::cout << std::endl;

  return 0;
}

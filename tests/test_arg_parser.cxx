#include <arg_parse/parser.hxx>

int main() {

  int argcv{2};
  char **argvv{{"test", "--help"}};

  auto install_command =
      Command::Syntax("install")
          .with_option(
              'f', "force",
              "Ignores any warnings and forces the operation to complete.")
          .with_optional_arg('C', "compare-with",
                             "Compares the package supplied metadata.",
                             Argument("comparison"))
          .with_optional_arg('B', "backup-old",
                             "backs up all the old metadata.",
                             Argument("backup-store"))
          .with_arg("package");

  ArgParser arg_parser("test");

  arg_parser.edit_root_command()
      .with_option('v', "version", "Prints the version of the program")
      .with_option('h', "help", "Shows the help screen");

  arg_parser.add_subcommand(install_command);
  if (arg_parser.parse(argcc[0], argvv[0])) {

  } else {
    arg_parser.print_help();
  }

  return 0;
}

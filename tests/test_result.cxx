#include <core/result.hxx>
#include <core/string.hxx>
#include <core/error.hxx>

struct Sauce{
  String flavour;

  Sauce(String &&str): flavour(std::move(str)) {}
  Sauce(const String& str): flavour(str) {}
  ~Sauce() {};
} ;

struct SauceIssue {
  
  enum class SubType {
    TOO_SWEET,
    DISGUISTING, 
    COUNT,
  } sub_type;


  SauceIssue(SubType st, String &&msg): sub_type(st), message(std::move(msg)) {}
  SauceIssue(SubType st, const String &msg): sub_type(st), message(msg) {}

  static const char* static_message(SubType type) {
    return error_static_messages(
      type,
      "The sauce is too sweet!",
      "The sauce is unbearable!"
      );
  } 

  String to_string() {
    return static_message(sub_type);
  }

private:
  String message;
};

Result<Sauce, SauceIssue> get_sauce() {
  return Sauce("Sweet-and-sour");
}

Result<Sauce, SauceIssue> get_error_sauce() {
  return Error<SauceIssue>(SauceIssue::SubType::DISGUISTING, "This is disguisting!");
}

int main (int argc, char** argv) {

  auto r = get_sauce();
  auto e = get_error_sauce();

  std::cout << "SAUCE1 FLAVOUR: " << r->flavour << std::endl;
  std::cout << "SAUCE1 Error: " << r.error_string() << std::endl;
  std::cout << "SAUCE2 ERROR: " << e.error_string() << std::endl;

  return 0;
}

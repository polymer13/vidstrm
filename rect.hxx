#ifndef __RECT_H_
#define __RECT_H_
#include <stdint.h>

struct Rect {
  uint32_t width, height;
  int32_t x, y;

  Rect();
  Rect(uint32_t w, uint32_t h, int32_t x, int32_t y);

  void set_top(int32_t y);
  void set_bottom(int32_t y);
  void set_left(int32_t x);
  void set_right(int32_t x);

  void resize(uint32_t w, uint32_t h);
  void place(int32_t nx, int32_t ny);
};

#endif  // __RECT_H_

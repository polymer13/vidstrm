#include "shader.hxx"
#include "core/file.hxx"
#include <GL/gl.h>

using namespace shader;
using Result<ShaderProgram, ShaderIssue> = ShaderProgram::ResultValue;
using ShaderProgramError = ShaderProgram::ErrorValue;

ShaderProgram::ShaderProgram(GLuint id)
    : m_id(id) {
}

Result<ShaderProgram, ShaderIssue> ShaderProgram::create() {
  auto id = glCreateProgram();

  if(id == 0) {
    return ShaderProgramError (
      ShaderIssue::SubType::SHADER_PROGRAM_CREATE,
      String("Could not create shader program, check your OpenGL driver installation.")
    );
  }

  return ShaderProgram(id);
}

// FIXME: Stop using the -> operator its inefficient
Result<ShaderProgram, ShaderIssue> ShaderProgram::link(VertexShader vs, FragmentShader fs) {

  auto sp = ShaderProgram::create();
  if(!sp) return sp;

  glAttachShader(sp->m_id, vs.get_id());
  glAttachShader(sp->m_id, fs.get_id());

  GLint link_status = 0;

  glLinkProgram(sp->m_id);

  glDetachShader(sp->m_id, vs.get_id());
  glDetachShader(sp->m_id, fs.get_id());

  glGetProgramiv(sp->m_id, GL_LINK_STATUS, &link_status);

  if (link_status == GL_FALSE) {
    GLint log_size = 0;
    String message;

    glGetProgramiv(sp->m_id, GL_INFO_LOG_LENGTH, &log_size);

    message.expand(log_size);

    glGetProgramInfoLog(sp->m_id, log_size, &log_size, &message[0]);

    return ShaderProgramError (
      ShaderIssue::SubType::LINK_ISSUE,
      std::move(message)
    );
  }
}

ShaderProgram::~ShaderProgram() { glDeleteProgram(sp->m_id); }

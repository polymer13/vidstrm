#include "gl_framebuffer.hxx"

using GLF = GL_Framebuffer;

void GLF::init() {}
void GLF::reinit() {}
void GLF::close() {}
void GLF::copy(Pixmap const &pixels) {}
void GLF::resize_notify() {}
